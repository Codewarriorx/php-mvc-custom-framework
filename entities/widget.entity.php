<?php
	class Widget{
		use EntityBasics; // includes getID, toArray, and toJSON methods
		private $id, $name;

		public function __construct($name, $id = null){
			$this->name = $name;
			$this->id = $id;
		}

		public function __destruct(){

		}

		public function getName(){
			return $this->name;
		}

		public function setName($value){
			$this->name = $value;
		}
	}
?>