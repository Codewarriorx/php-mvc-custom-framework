<?php
	// this is for error finding purposes
	// error_reporting(E_ALL);
	// ini_set( 'display_errors','1'); 


	require ("library/loader.class.php");
	/* nullify any existing autoloads */
    spl_autoload_register(null, false);

    /* specify extensions that may be loaded */
    spl_autoload_extensions('.php, .class.php, .trait.php, .model.php, .entity.php, view.php, controller.php');

    /* register the loader functions */
    spl_autoload_register('Loader::libLoader');
    spl_autoload_register('Loader::traitLoader');
    spl_autoload_register('Loader::entityLoader');
    spl_autoload_register('Loader::modelLoader');
    spl_autoload_register('Loader::controllerLoader');
    spl_autoload_register('Loader::viewLoader');

	// Set constants for error handling class
	define('FATAL', E_USER_ERROR);
	define('ERROR', E_USER_WARNING);
	define('WARNING', E_USER_NOTICE);

	// Require db credentials
	require("../webcredentials.php");

    // Database constants
	define ('DB_HOST', 'localhost');
	define ('DB_NAME', 'widgets');
	define ('DB_USER', $username);
	define ('DB_PASS', $password);

	// Create controller and execute actions
	$loader = new loader($_GET);
	$controller = $loader->createController();

	// Include header
	// include ("templates/header.php");

	$view = $controller->executeAction();
	$view->render();

	// Include footer
	include ("templates/footer.php");
?>