<?php
	abstract class BaseController{
		protected $urlValues;
		protected $action;

		public function __construct($action, $urlValues){
			$this->action = $action;
			$this->urlValues = $urlValues;
		}

		protected function postFormHandler(){
			foreach ($_POST as $field => $value){
				$temp = trim($value);
				if(empty($temp) && in_array($field, $this->required)){ // Check if field is empty and required
					array_push($this->errors, $field); // Error: field is either empty or required
				}
				$this->clean[$field] = $this->sanitize($value); // sanitize
			}

			if(empty($this->errors)){ // If true there were no errors
				return true; // no errors
			}
			else{
				return false;
			}
		}

		protected function sanitize($string){
			$string = trim($string);
			$string = stripslashes($string);
			$string = htmlentities($string);
			$string = strip_tags($string);
			return $string;
		}

		public function executeAction(){
			return $this->{$this->action}();
		}

		protected function returnAll(){
			return $this->queryDB('SELECT * FROM tags','');
		}

		protected function returnView($viewModel, $fullView){
			$viewLoc = 'views/'.get_class($this).'/'.$this->action.'.php';

			if($fullView){
				require ("views/mainTemplate.php");
			}
			else{
				require ($viewLoc);
			}
		}
	}

?>