<?php
	abstract class BaseView{
		protected $errorMessage;

		public function __construct(){
			$this->errorMessage = null;
		}

		protected function render(){
			require("newinside/global/headers.php");
			include ("templates/header.php");
		}

		protected function createErrorMessage($message){
			ob_start();
			require("templates/generic/errorMessage.tpl.php");
			return ob_get_clean();
		}

	}

?>