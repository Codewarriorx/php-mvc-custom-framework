<?php

class ErrorHandler
{
    private $debug = 0;

    public function __construct($debug = 0)
    {
        $this->debug = $debug;
        set_error_handler(array($this, 'handleError'));
    }

    public function handleError($errorType, $errorString, $errorFile, $errorLine)
    {	
        switch ($errorType)
        {
            case FATAL:
	            switch ($this->debug)
	            {
					case 0:
	                    echo 'Sadly an error has occured!';
	                    exit;
	                case 1:
	                    echo "<pre><b>FATAL</b> [T: $errorType] [L: $errorLine] [F: $errorFile]<br/>$errorString<br /></pre>";
	                    exit;
	            }
            case ERROR:
	            echo "<pre><b>ERROR</b> [T: $errorType] [L: $errorLine] [F: $errorFile]<br/>$errorString<br /></pre>";
	            break;
            case WARNING:
	            echo "<pre><b>WARNING</b> [T: $errorType] [L: $errorLine] [F: $errorFile]<br/>$errorString<br /></pre>";
	            break;
        }
    }
} 

?>