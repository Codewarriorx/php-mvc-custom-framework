<?php
	class WidgetsModel extends DataAccessHandler{
		use Crud; // uses trait class crud
		
		private $selectAllQuery = 'SELECT * FROM widgets';
		private $selectOneQuery = 'SELECT * FROM widgets WHERE id = :id';
		private $deleteQuery = 'DELETE FROM widgets WHERE id = :id';
		private $insertQuery = 'INSERT INTO widgets (name) VALUES (:name)';
		private $updateQuery = 'UPDATE widgets SET name = :name WHERE id = :id';

		public function __construct(){
			parent::__construct();
		}

		public function __destruct(){
			parent::__destruct();
		}

		public function buildEntities($data){
			$entities = [];
			foreach ($data as $row) {
				$tempEntity = new Widget($row['name'], $row['id']);
				array_push($entities, $tempEntity);
			}
			return $entities;
		}
	}
?>