<div class="tableContainer">
	<form action="#" method="POST">
		<table class='dataTable' width="100%">
			<thead>
				<tr>
					<?=$headerColumns?>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?=$body?>
			</tbody>
		</table>
	</form>
</div>