<?php
	trait Crud{
		public function read($id = null){
			if(is_null($id)){
				$results = $this->queryDB($this->selectAllQuery);
			}
			else{
				$results = $this->queryDB($this->selectOneQuery, $id); // should turn id into array
			}
			return $this->buildEntities($results);
		}

		public function delete($entities){
			foreach ($entities as $entity) {
				$this->updateDB( $this->deleteQuery, array('id' => $entity->getID()) );
			}
		}

		public function update($entities){
			foreach ($entities as $entity) {
				$this->updateDB($this->updateQuery, $entity->toArray());
			}
		}

		public function insert($entities){
			foreach ($entities as $entity) {
				$data = $entity->toArray();
				unset($data['id']); // remove id since it will always be unneeded for inserts
				$this->updateDB($this->insertQuery, $data);
			}
		}
	}
?>