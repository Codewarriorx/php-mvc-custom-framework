<?php
	trait EntityBasics{
		public function getID(){
			return $this->id;
		}

		public function toArray(){
			return get_object_vars($this);
		}

		public function toJSON(){
			return json_encode($this->toArray());
		}
	}
?>