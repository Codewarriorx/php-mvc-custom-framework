<?php
	class ImageView{
		private $image;

		public function __construct($image){
			$this->image = $image;
		}

	    public function render() {
	        header("Content-type: image/jpeg");
        	echo $this->image;
	    }
	}
?>