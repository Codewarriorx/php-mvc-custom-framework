<?php
	class JsonView{
		private $entityList;

		public function __construct($entityList){
			$this->entityList = $entityList;
		}

	    public function render() {
	        header('Content-Type: application/json; charset=utf8');
	        
	        $info = array();
	        foreach ($this->entityList as $entity) {
	        	array_push($info, $entity->toArray());
	        }

	        echo json_encode(array('entity' => $info));
	        return true;
	    }
	}
?>