<?php
	class EditView{
		private $categoryEntity, $tagEntities, $allTags, $heading, $action;

		public function __construct($heading, $action, $tagEntities, $allTags, $categoryEntity = null){
			$this->heading = $heading;
			$this->action = $action;
			$this->categoryEntity = $categoryEntity;
			$this->tagEntities = $tagEntities;
			$this->allTags = $allTags;
		}

		public function render(){
			if(!is_null($this->categoryEntity)){
				$values = $this->categoryEntity->toArray();
			}
			else{
				$values = array();
			}
			
			$tags = array();
			$allTags = array();

			foreach ($this->tagEntities as $entity) {
				$tags[$entity->getID()] = $entity->getName();
			}

			foreach ($this->allTags as $entity) {
				if(!isset($tags[$entity->getID()])){
					$allTags[$entity->getID()] = $entity->getName();
				}
			}

			$heading = $this->heading;
			$action = $this->action;
			include('templates/categories/editForm.tpl.php');
		}
	}
?>