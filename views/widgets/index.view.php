<?php
	class IndexView extends BaseView{
		private $catEntityList;

		public function __construct($catEntityList){
			parent::__construct();
			$this->catEntityList = $catEntityList;
		}

		public function render(){
			parent::render();
			$url = '/smecAdmin/categories/addForm';
			$header = 'Categories';
			$content = $this->buildTable("Categories", "1");
			include("templates/generic/section.tpl.php");

			// add js
			ob_start();
			require("templates/categories/javascript.tpl.js");
			$javascript = ob_get_clean();

			ob_start();
			include("templates/categories/functions.tpl.js");
			$functions = ob_get_clean();

			include("templates/generic/pageJS.tpl.php");
		}

		private function buildTable($header, $tableNum){
			$headerColumns = $this->createTableHeader();
			$body = $this->createTableBody();
			$url = '/smecAdmin/categories/addForm';
			ob_start();
			include("templates/generic/table.tpl.php");
			return ob_get_clean();
		}

		private function createTableHeader(){
			$header = "";
			$info = $this->catEntityList[0]->toArray();
			foreach ($info as $text => $value) {
				ob_start();
				include("templates/generic/tableHeaderCol.tpl.php");
				$content = ob_get_clean();
				$header .= $content;
			}
			return $header;
		}

		private function createTableBody(){
			$body = "";
			$entityList = $this->catEntityList;
			foreach ($entityList as $entity) {
				ob_start();
				$values = $entity->toArray();
				include("templates/categories/tableBodyRow.tpl.php");
				$content = ob_get_clean();
				$body .= $content;
			}
			return $body;
		}
	}
?>